# Adventure-Game-2024-Loch

## Versions
- Unity [2022.3.23f1 (LTS)](unityhub://2022.3.23f1/dbb3f7c5b5c6)
- FMOD Studio [2.02 (Unity Verified)](https://www.fmod.com/download)

## Links
- Art [Asset List]  [Documentation](https://docs.google.com/spreadsheets/d/1ZOlA2UKqNCFzyyNiZHvT-X3DLCLqptQ800N0KvuEctg/edit?usp=sharing)
- Sound Asset List [TODO](TODO)
- Kanban Board [TODO](TODO)
- Super Behaviour [Documenttaion](https://superbehaviour.lom.li/api/index.html)
- DoTween [Documentation](https://dotween.demigiant.com/documentation.php)

## Diagrams
- Miro [Documentation](https://miro.com/app/board/uXjVKY_xCzo=/?share_link_id=326002641749)


## Important Usings
```csharp
using LoM.Super;
using TMPro;
using DG.Tweening;
```

## Git Procedure
- Pull first
- Push second
- this is to avoid and any conflicts in the repository